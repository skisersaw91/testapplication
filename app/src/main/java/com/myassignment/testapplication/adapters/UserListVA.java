package com.myassignment.testapplication.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myassignment.testapplication.R;
import com.myassignment.testapplication.models.User;
import com.myassignment.testapplication.viewHolder.UserListVH;

import java.util.ArrayList;
import java.util.List;

public class UserListVA extends RecyclerView.Adapter<UserListVH> {
    List<User> mUserList = new ArrayList<>();
    Context mContext;

    public UserListVA(List<User> mUserList, Context mContext) {
        this.mUserList = mUserList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public UserListVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.userlist_list_item,null);
        UserListVH viewHolder = new  UserListVH(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserListVH userListVH, int i) {
        final User mUserItem = mUserList.get(i);


        try{

            userListVH.id_tv.setText(mUserItem.getId());
            userListVH.name_tv.setText(mUserItem.getName());

        }catch (Exception e){
            Log.e("UserListVA","Error : " + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public void updateData(List<User> newUserList){

        this.mUserList = newUserList;
        notifyDataSetChanged();

    }
}
