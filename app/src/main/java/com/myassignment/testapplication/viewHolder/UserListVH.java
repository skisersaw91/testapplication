package com.myassignment.testapplication.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.myassignment.testapplication.R;

public class UserListVH extends RecyclerView.ViewHolder {

    public TextView id_tv;
    public TextView name_tv;

    public UserListVH( View itemView){
        super(itemView);

        id_tv = (TextView) itemView.findViewById(R.id.uid_tv);
        name_tv = (TextView) itemView.findViewById(R.id.name_tv);
    }
}
