package com.myassignment.testapplication;

import android.content.Context;
import android.graphics.ColorSpace;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.myassignment.testapplication.adapters.UserListVA;
import com.myassignment.testapplication.models.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String TAG = this.getClass().getSimpleName();

    User user01,user02,user03,user04;
    List<User> mUserArraylist = new ArrayList<>();
    Context mContext;
    UserListVA mUserListVA;

    //UI
    Spinner sortSpinner;
    RecyclerView userList_rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        try{

            initUI();
            initData();


        }catch (Exception e){

            Log.e(TAG,"Error : " + e.getMessage());

        }

    }

    private void initData() {
        user01 = new User("1","Shiro");
        user02 = new User("2","Lina");
        user03 = new User("3","Wakaba");
        user04 = new User("4","Rin");

        mUserArraylist.add(user01);
        mUserArraylist.add(user02);
        mUserArraylist.add(user03);
        mUserArraylist.add(user04);

        mUserListVA.updateData(mUserArraylist);
    }

    private void initUI(){

        sortSpinner = (Spinner)findViewById(R.id.sort_spinner);

        userList_rv = (RecyclerView)findViewById(R.id.userlist_rv);


        userList_rv.setHasFixedSize(true);

        userList_rv.setLayoutManager(new LinearLayoutManager(userList_rv.getContext(), LinearLayoutManager.VERTICAL,false));
        mUserListVA = new UserListVA(mUserArraylist,mContext);

        userList_rv.setAdapter(mUserListVA);


        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sortData();
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    private void sortData(){
        switch(sortSpinner.getSelectedItem().toString()){
            case "Sort Ascending by ID":
                Toast.makeText(mContext,
                        "Sort Ascending by ID",
                        Toast.LENGTH_SHORT).show();

                Collections.sort(mUserArraylist, new Comparator<User>() {
                    @Override
                    public int compare(User user01, User user02) {
                        return user01.getId().compareTo(user02.getId());
                    }
                });


                mUserListVA.updateData(mUserArraylist);
                break;

            case "Sort Ascending by Name":

                Toast.makeText(mContext,
                        "Sort Ascending by Name",
                        Toast.LENGTH_SHORT).show();

                Collections.sort(mUserArraylist, new Comparator<User>() {
                    @Override
                    public int compare(User user01, User user02) {
                        return user01.getName().compareTo(user02.getName());
                    }
                });

                mUserListVA.updateData(mUserArraylist);
                break;
        }
    }
}
